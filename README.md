# Description
Starts up the PC (Server) after shutdown at a specific time of day.

When stopped before 4:00, it will restart at 12:00 at the *same* day.
When stopped after 4:00, it will restart at 12:00 the *next* day.

Idea/Technology from [here](https://www.linux.com/training-tutorials/wake-linux-rtc-alarm-clock/)

# Prerequisits

	apt install python3

# Installation
copy the two scripts in /usr/local/bin

# Usage
execute the shell script every 5 minutes

	*/5 * * * *	root	/usr/local/bin/setWakealarm.sh

# Test
You can see the timestamp of planned restart when you execute

	cat /sys/class/rtc/rtc0/wakealarm
