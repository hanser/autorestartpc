import time
import datetime

dt = datetime.datetime.now().timetuple()

addition = 0

if dt.tm_hour > 3:
    # wenn später als 4 Uhr -> erst am nächsten Tag starten
    addition = 86400

wakeuptime = datetime.datetime(dt.tm_year, dt.tm_mon, dt.tm_mday, 12, 0)
timestamp = int(time.mktime(wakeuptime.timetuple())) + addition
print(timestamp)
# datetime.datetime.fromtimestamp(timestamp)
